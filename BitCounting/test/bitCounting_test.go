package test

import (
	"katas/BitCounting/src"
	"testing"
)

func TestBitCounting(t *testing.T) {
	assertion := 5
	num := 1234
	res := src.BitCounting(uint(num))

	if assertion != res {
		t.Errorf("Result %d not equal to assertion %d", res, assertion)
	}

}
