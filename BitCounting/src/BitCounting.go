package src

import (
	"strconv"
)

func reverse(str string) (result string) {
	for _, l := range str {
		result = string(l) + result
	}
	return result
}

func convertToBinary(x uint, s string) string {
	if x == 0 {
		return reverse(s)
	}
	reminder := x % 2
	s += strconv.Itoa(int(reminder))
	return convertToBinary(x/2, s)
}

func BitCounting(x uint) (res int) {
	binary_str := convertToBinary(x, "")
	for _, b := range binary_str {
		if b == '1' {
			res += 1
		}
	}
	return res
}
