package test

import (
	"katas/DuplicateEncoder/src"
	"testing"
)

func TestDuplicateEncoder(t *testing.T) {
	examples := []string{"din", "recede", "(( @", "Success", "())", "Prespecialized"}
	assertions := []string{"(((", "()()()", "))((", ")())())", "())", ")()())()(()()("}
	for i, n := range examples {
		if src.DuplicateEncoder(n) != assertions[i] {
			t.Error("Result didn't match expected value")
		}
	}

}
