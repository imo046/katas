package src

import (
	"strings"
)

func DuplicateEncoder(word string) string {
	countMap := make(map[string]int)
	lowerCaseWord := strings.ToLower(word)
	var res string
	for _, e := range strings.Split(lowerCaseWord, "") {
		countMap[e]++
	}
	for i := 0; i < len(word); i++ {
		e := string(lowerCaseWord[i])
		if countMap[e] > 1 {
			res = res + ")"
		} else {
			res = res + "("
		}
	}
	return res
}
