package test

import (
	"katas/BouncingBall/src"
	"testing"
)

type ValueStruct struct {
	h      float64
	bounce float64
	window float64
}

func TestBouncingBall(t *testing.T) {
	param := []ValueStruct{{3, 0.66, 1.5}, {40, 0.4, 10}}
	assertions := []int{3, 3}
	for i, e := range assertions {
		data := param[i]
		res := src.BouncingBall(data.h, data.bounce, data.window)
		if res != e {
			t.Errorf("Result %v didn't match expected value %v", res, assertions[i])
		}
	}
}
