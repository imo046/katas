package src

func BouncingBall(h, bounce, window float64) int {
	if h > 0 && bounce > 0 && bounce < 1 && window < h {
		b_h := h
		t_s := 1
		for {
			b_h *= bounce
			if b_h < window {
				break
			}
			t_s += 2
		}
		return t_s
	} else {
		return -1
	}
}
