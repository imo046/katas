package test

import (
	"katas/Parentheses/src"
	"testing"
)

func TestValidParentheses(t *testing.T) {
	examples := []string{"(())((()((()))))", "()", ")(()))", "(", "(())((()())())"}
	assertions := []bool{true, true, false, false, true}
	for i, n := range examples {
		if src.ValidParentheses(n) != assertions[i] {
			t.Error("Result didn't match expected value")
		}
	}

}
