package src

import "strings"

func ValidParentheses(parens string) bool {
	var rCount, lCount int
	if strings.HasPrefix(parens, ")") || strings.HasSuffix(parens, "(") {
		return false
	}
	for _, p := range strings.Split(parens, "") {
		switch {
		case p == "(":
			rCount += 1
		case p == ")" && rCount == 1 && lCount == 1:
			return false
		case p == ")":
			lCount += 1
		}
	}
	return rCount == lCount
}
