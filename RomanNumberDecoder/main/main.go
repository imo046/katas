package main

import (
	"fmt"
	"katas/RomanNumberDecoder/src"
)

func main() {

	roman_nums := []string{"XXI", "MMVIII", "IX", "V", "MMMMCDVI"}
	for _, n := range roman_nums {
		fmt.Println(src.Decode(n))
	}

}
