package src

import "strings"

var Symbols = map[string]int{
	"I": 1,
	"V": 5,
	"X": 10,
	"L": 50,
	"C": 100,
	"D": 500,
	"M": 1000,
}

func Decode(roman string) int {
	nums := strings.Split(roman, "")
	if len(nums) <= 1 {
		return Symbols[nums[0]]
	}
	var base int
	var head int
	var next int
	nums_len := len(nums)
	for i, n := range nums {
		head = Symbols[n]
		if i >= nums_len-1 {
			base = base + head
			return base
		}
		next = Symbols[nums[i+1]]

		if head >= next {
			base = base + head
		} else {
			base = base - head
		}
	}
	return base
}
