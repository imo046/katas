package test

import (
	"katas/RomanNumberDecoder/src"
	"testing"
)

func TestDecodeRoman(t *testing.T) {
	roman_nums := []string{"XXI", "MMVIII", "IX", "V", "MMMMCDVI"}
	assertions := []int{21, 2008, 9, 5, 4406}
	for i, n := range roman_nums {
		if src.Decode(n) != assertions[i] {
			t.Error("Result didn't match expected value")
		}
	}

}
