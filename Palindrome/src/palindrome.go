package src

import (
	"strings"
)

func IsPalindrome(str string) bool {
	l := len(str)
	if l <= 1 {
		return true
	}
	j := l - 1
	pArr := strings.Split(strings.ToLower(str), "")
	for i := 0; i <= j; i++ {
		if pArr[i] != pArr[j] {
			return false
		}
		j--
	}
	return true
}
