package test

import (
	"katas/Palindrome/src"
	"testing"
)

func TestIsPalindrome(t *testing.T) {
	examples := []string{"din", "aba", "(( @", "Succus", "()()", "Prespecialized"}
	assertions := []bool{false, true, false, true, false, false}
	for i, n := range examples {
		if src.IsPalindrome(n) != assertions[i] {
			t.Error("Result didn't match expected value")
		}
	}

}
