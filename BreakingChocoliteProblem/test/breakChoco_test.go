package test

import (
	"katas/BreakingChocoliteProblem/src"
	"testing"
)

type Inputs struct {
	input []int
}

func TestBreakChoco(t *testing.T) {
	assertions := []int{24, 0}
	inputs := []Inputs{{[]int{5, 5}}, {[]int{1, 1}}}
	for i, n := range assertions {
		x, y := inputs[i].input[0], inputs[i].input[1]
		res := src.BreakChoco(x, y)
		if res != n {
			t.Errorf("Result %d is not equal to assertion %d", res, n)
		}
	}
}
