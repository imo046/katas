package src

import (
	"strconv"
)

func Persistence(n int) int {
	attempts := 0
	var recF func(int, *int)

	recF = func(j int, i *int) {
		strRepr := strconv.Itoa(j)
		if len(strRepr) <= 1 {
			return
		}
		*i += 1
		res := 1
		for _, r := range strRepr {
			num, err := strconv.Atoi(string(r))
			if err != nil {
				panic(err)
			}
			res *= num
		}
		recF(res, i)
	}

	recF(n, &attempts)

	return attempts
}
