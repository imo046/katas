package src

import "strings"

func SpinWords(str string) string {
	if len(str) < 5 {
		return str
	}
	var reverseStr func([]string, string) string
	var acc []string
	reverseStr = func(acc []string, s string) string {
		l := len(s)
		if l <= 0 {
			return strings.Join(acc, "")
		}
		acc = append(acc, s[l-1:])
		return reverseStr(acc, s[:l-1])
	}
	words := strings.Split(str, " ")
	for i, w := range words {
		if len(w) >= 5 {
			words[i] = reverseStr(acc, w)
		}
	}
	return strings.Join(words, " ")
}
