package test

import (
	"katas/ReverseWords/src"
	"testing"
)

func TestSpinWords(t *testing.T) {
	examples := []string{"Hey fellow warriors", "This is the test", " one three"}
	assertions := []string{"Hey wollef sroirraw", "This is the test", " one eerht"}
	for i, e := range examples {
		if src.SpinWords(e) != assertions[i] {
			t.Errorf("Result %v didn't match expected value %v", e, assertions[i])
		}
	}
}
