package main

import (
	"fmt"
	"strings"
)

func main() {

	var reverseStr func([]string, string) string
	reverseStr = func(acc []string, s string) string {
		l := len(s)
		if l <= 0 {
			return strings.Join(acc, "")
		}
		acc = append(acc, s[l-1:])
		return reverseStr(acc, s[:l-1])
	}

	fmt.Println(reverseStr([]string{}, "abcd"))

}
