package src

func Abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func Gcdi(x, y int) int {
	var res int
	x, y = Abs(x), Abs(y)
	if y > x {
		res = y
	} else {
		res = x
	}
	for res > 0 {
		if x%res == 0 && y%res == 0 {
			break
		}
		res = res - 1
	}
	return res
}

func Lcmu(x, y int) int {
	x, y = Abs(x), Abs(y)
	var res int
	var output int
	if x > y {
		res = x
	} else {
		res = y
	}
	for {
		if res%x == 0 && res%y == 0 {
			output = res
			break
		}
		res += 1
	}
	return output
}

func Som(x, y int) int {
	return x + y
}

func Maxi(x, y int) int {
	if x > y {
		return x
	}
	return y
}

func Mini(x, y int) int {
	if x < y {
		return x
	}
	return y
}

type FParam func(int, int) int

func OperArray(f FParam, arr []int, init int) []int {
	for i, _ := range arr {
		if i == 0 {
			arr[i] = f(init, arr[i])
		} else {
			arr[i] = f(arr[i-1], arr[i])
		}
	}
	return arr
}
