package test

import (
	"katas/ReducingBySteps/src"
	"reflect"
	"testing"
)

func TestGcdi(t *testing.T) {
	assertion1 := 17
	res1 := src.Gcdi(34, 17)

	assertion2 := 14
	res2 := src.Gcdi(98, 56)

	if assertion1 != res1 {
		t.Errorf("Result %d not equal to assertion %d", res1, assertion1)
	}
	if assertion2 != res2 {
		t.Errorf("Result %d not equal to assertion %d", res2, assertion2)
	}

}

func TestLcmu(t *testing.T) {
	assertion := 60
	res := src.Lcmu(15, 20)
	if assertion != res {
		t.Errorf("Result %d not equal to assertion %d", res, assertion)
	}
}

func TestOpenArray(t *testing.T) {
	arr_gcdi := []int{18, 69, -90, -78, 65, 40}
	arr_som := []int{357, 112, 28, -52, 644, 119}
	arr_lcmu := []int{18, 69, -90, -78, 65, 40}

	sol_gcdi := []int{18, 3, 3, 3, 1, 1}
	sol_som := []int{357, 469, 497, 445, 1089, 1208}
	sol_lcmu := []int{18, 414, 2070, 26910, 26910, 107640}

	res_gcdi := src.OperArray(src.Gcdi, arr_gcdi, arr_gcdi[0])
	res_som := src.OperArray(src.Som, arr_som, 0)
	res_lcmu := src.OperArray(src.Lcmu, arr_lcmu, arr_lcmu[0])

	if !reflect.DeepEqual(sol_gcdi, res_gcdi) {
		t.Errorf("Result %v for gcdi not equal assertion %v", res_gcdi, sol_gcdi)

	}
	if !reflect.DeepEqual(sol_som, res_som) {
		t.Errorf("Result %v for som not equal assertion %v", res_som, sol_som)

	}
	if !reflect.DeepEqual(sol_lcmu, res_lcmu) {
		t.Errorf("Result %v for lcmu not equal assertion %v", res_lcmu, sol_lcmu)

	}

}
