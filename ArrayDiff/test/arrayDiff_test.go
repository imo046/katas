package test

import (
	"katas/ArrayDiff/src"
	"reflect"
	"testing"
)

type Arrays struct {
	a []int
	b []int
}

func TestArrayDiff(t *testing.T) {
	emptyArr := make([]int, 0)
	assertions := [][]int{[]int{1}, []int{2, 2}, []int{2, 3, 3}, []int{}}
	arr_inputs := []Arrays{Arrays{[]int{1, 2, 2}, []int{2}}, Arrays{[]int{1, 2, 2, 3}, []int{1, 3}}, {[]int{2, 1, 3, 1, 3}, []int{1}}, {emptyArr, []int{1, 2}}}
	for i, n := range assertions {
		a, b := arr_inputs[i].a, arr_inputs[i].b
		res := src.ArrayDiff(a, b)
		if !reflect.DeepEqual(n, res) {
			t.Errorf("Result %v is not equal to assertion %v", res, n)
		}
	}
}
