package src

func contains(e int, b []int) bool {
	for _, n := range b {
		if n == e {
			return true
		}
	}
	return false
}

func ArrayDiff(a, b []int) []int {
	c := make([]int, 0)
	if len(a) < 1 {
		return a
	}
	for _, n := range a {
		if !contains(n, b) {
			c = append(c, n)
		}
	}
	return c
}
