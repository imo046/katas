package src

import (
	"fmt"
	"strconv"
	"strings"
)

func unpackSlice(slc []uint) string {
	var s strings.Builder
	for _, x := range slc {
		s.WriteString(strconv.Itoa(int(x)))
	}
	return s.String()
}

func CreatePhoneNumber(numbers [10]uint) string {
	head := unpackSlice(numbers[:3])
	body := unpackSlice(numbers[3:6])
	tail := unpackSlice(numbers[6:10])
	return fmt.Sprintf("(%s) %s-%s", head, body, tail)
}
