package test

import (
	"katas/phoneNumber/src"
	"testing"
)

func TestCreatePhoneNumber(t *testing.T) {
	numbers := [10]uint{1, 2, 3}
	assertion := "(123) 000-0000"
	res := src.CreatePhoneNumber(numbers)
	if res != assertion {
		t.Error("Result didn't match expected value")
	}

}
